server {
  listen 80;
  server_name media;
  root /var/www/html/public;
  client_max_body_size 80M;

  access_log /var/log/nginx/laravel.access.log;
  error_log /var/log/nginx/laravel.error.log;
  
  # Enable compression, this will help if you have for instance advagg‎ module
  # by serving Gzip versions of the files.
  gzip_static on;

  location = /favicon.ico {
          log_not_found off;
          access_log off;
  }

  location = /robots.txt {
          deny all;
          log_not_found off;
          access_log off;
  }

  # Very rarely should these ever be accessed outside of your lan
  location ~* \.(txt|log)$ {
          allow 192.168.0.0/16;
          deny all;
  }

  location ~ \..*/.*\.php$ {
          return 403;
  }

  # No no for private
  location ~* /sites/default/files/private {
          internal;
  }

  ## Regular private file serving (i.e. handled by Drupal).
  location ^~ /system/files/ {
      ## For not signaling a 404 in the error log whenever the
      ## system/files directory is accessed add the line below.
      ## Note that the 404 is the intended behavior.
      log_not_found off;
      access_log off;
      expires 30d;
      try_files $uri @rewrite;
  }


  # Block access to "hidden" files and directories whose names begin with a
  # period. This includes directories used by version control systems such
  # as Subversion or Git to store control files.
  location ~ (^|/)\. {
          return 403;
  }

  location / {
          # This is cool because no php is touched for static content
          try_files $uri @rewrite;
  }

  location @rewrite {
          # For D7 and above:
          # Clean URLs are handled in drupal_environment_initialize().
          rewrite ^ /index.php;
  }

  location ~ \.php$ {
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    #NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini
    include fastcgi_params;
    fastcgi_buffer_size 32k;
    fastcgi_buffers 64 4k;
    fastcgi_busy_buffers_size 224k;
    fastcgi_pass php:9000;
    fastcgi_param SCRIPT_FILENAME $request_filename;
    fastcgi_intercept_errors on;
  }

  # Fighting with Styles? This little gem is amazing.
  # This is for D7 and D8
  location ~ ^/sites/.*/files/styles/ {
          try_files $uri @rewrite;
  }

  location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
          expires max;
          log_not_found off;
  }
    
  # deny access to .htaccess files, if Apache's document root
  # concurs with nginx's one    
  location ~ /\.ht {
    deny  all;
  }    
}
